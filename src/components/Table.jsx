import React, { useState, useEffect } from 'react'

function Table({ userData, apiData }) {
  useEffect(() => {
    const id = data[data.length - 1] ? data[data.length - 1].id + 1 : 1;
    if (userData.length) userData[0].id = id;
      const newData = [...data, ...userData];
      setData(newData);
  },[userData])

  const [data, setData] = useState([])

  function exchangeRate(currentExchangeRate) {
    const rate = Object.keys(apiData).findIndex((item) => item === currentExchangeRate)
    const value = Object.values(apiData)[rate]
    return value.ask
  }

  function convertToBRL(value, currentExchangeRate) {
    return (value * currentExchangeRate).toFixed(2)
  }

  function handleDeleteClick(id) {
    const newUserData = data.filter((user) => user.id !== id)
    setData(newUserData)
  }


  return (
    <div
      title="table"
      className="w-full max-w-md mx-auto rounded overflow-hidden  mt-10"
    >
      {
        data?.map((user) => {
          return (
            <div
              className="flex align-center items-center justify-between p-2"
              key={user.id}
            >
              <span data-testid="value">{Number(user.value).toFixed(2)}</span>
              <span data-testid="description">{user.description}</span>
              <span data-testid="currency">{user.currency}</span>
              <span data-testid="exchangeRate">{ exchangeRate(user.currency) }</span>
              <span data-testid="brlValue">{ convertToBRL(user.value, exchangeRate(user.currency)) }</span>
              <button
                className="border-2 rounded-lg p-2
        bg-red-500 hover:bg-red-700 text-white font-bold px-4
        cursor-pointer focus:outline-none focus:shadow-outline"
                data-testid="delete-btn"
                onClick={() => handleDeleteClick(user.id)}
              >
                Delete
              </button>
            </div>
          )
        })
      }
    </div>
  )
}

export default Table