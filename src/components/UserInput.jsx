import React from 'react'
import { useState } from 'react';

function UserInput({ currencies, userInputs }) {
  const [value, setValue] = useState('')
  const [description, setDescription] = useState('')
  const [currency, setCurrency] = useState('USD')

  function addEntrance() {
    userInputs(value, description, currency);
    setValue('')
    setDescription('')
    setCurrency('USD')
  }

  return (
    <form>
      <input
        className="border-2 border-gray-400 rounded-lg p-2
        text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        type="number"
        value={value}
        aria-label="value"
        onChange={e => setValue(e.target.value)}
      />
      <input
        className="border-2 border-gray-400 rounded-lg p-2
        text-gray-700 leading-tight focus:outline-none focus:shadow-outline mx-2"
        type="text"
        value={description}
        aria-label="description"
        onChange={e => setDescription(e.target.value)}
      />
      <select
        className="border-2 border-gray-400 rounded-lg p-2
        text-gray-700 leading-tight focus:outline-none focus:shadow-outline mx-2"
        aria-label="currency"
        value={currency}
        onChange={e => setCurrency(e.target.value)}
      >
        { currencies && Object.keys(currencies).map((curr) => {
          return <option aria-label={curr} value={curr} key={curr}>{curr}</option>
        }) }
      </select>
      <button
        className="border-2 rounded-lg p-2
        bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4
        cursor-pointer focus:outline-none focus:shadow-outline"
        type="button"
        aria-label="Add"
        onClick={() => addEntrance()}
        disabled={ !value ? true : false }
      >
        Adicionar
      </button>
    </form>
  )
}

export default UserInput;