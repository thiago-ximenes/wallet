import React from 'react';
import { render } from '@testing-library/react';
import axios from 'axios';
import data from './mocks/data';
import api from '../services/api';
import App from '../App';

jest.mock('axios');

describe('Should ', () => {
  it('Render App and request data once', async () => {
    render(<App />);

    expect(axios.get).toBeCalledTimes(1);
  });
  it('return the correct data when call api function', async () => {
    axios.get.mockImplementationOnce(() => Promise.resolve(data));

    const result = await api();

    expect(result).toEqual(data);
  });
});