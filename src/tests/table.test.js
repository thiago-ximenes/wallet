import React, {useState} from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Table from '../components/Table';
import {data} from './mocks/data';
import {userData} from './mocks/userData';


describe('Render user entrances', () => {
  beforeEach(() => render(<Table userData={userData} apiData={data} />));
  it('There is a value field', () => {
    let spans = screen.getAllByTestId('value');

    spans.forEach((span) => {
      expect(span).toBeInTheDocument();
    })
    expect(spans.length).toBe(userData.length);
    expect(spans[0]).toHaveTextContent('100.00');
    expect(spans[1]).toHaveTextContent('200.00');

  });
  it('Theres is a description field', () => {
    const spans = screen.getAllByTestId('description');

    spans.forEach((span) => {
      expect(span).toBeInTheDocument();
    })
    expect(spans.length).toBe(userData.length);
    expect(spans[0]).toHaveTextContent('teste');
    expect(spans[1]).toHaveTextContent('');
  });
  it('There is a currency field', () => {
    const spans = screen.getAllByTestId('currency');

    spans.forEach((span) => {
      expect(span).toBeInTheDocument();
    });
    expect(spans.length).toBe(userData.length);
    expect(spans[0]).toHaveTextContent('USD');
    expect(spans[1]).toHaveTextContent('CAD');
  });
  it('There is a currency exchange rate field', () => {
    const spans = screen.getAllByTestId('exchangeRate');
    const currencySpans = screen.getAllByTestId('currency');

    spans.forEach((span, index) => {
      expect(span).toBeInTheDocument();
      const exchangeRateKey = Object.keys(data)
        .findIndex((currency) => currency === currencySpans[index].textContent);
      const exchangeRateValue = Object.values(data)[exchangeRateKey];
      expect(span).toHaveTextContent(exchangeRateValue.ask);
    });
    expect(spans.length).toBe(userData.length);
  });
  it('There is a BRL converted value', () => {
    const spans = screen.getAllByTestId('brlValue');
    const currencySpans = screen.getAllByTestId('currency');
    const value = screen.getAllByTestId('value');

    spans.forEach((span, index) => {
      expect(span).toBeInTheDocument();
      const exchangeRateKey = Object.keys(data)
        .findIndex((currency) => currency === currencySpans[index].textContent);
      const exchangeRateValue = Object.values(data)[exchangeRateKey];
      expect(span).toHaveTextContent((exchangeRateValue.ask * Number(value[index].textContent)).toFixed(2));
    });
  });
});

describe('Should not render with no user data', () => {
  it('', () => {
    render(<Table userData={[]} apiData={data} />);
    const div = screen.getByTitle('table');
    
    expect(div).toBeEmptyDOMElement();
  });
});

describe('Should has a delete button on every entrance', () => {
  beforeEach(() => render(<Table userData={userData} apiData={data} />));
  it('there is a delete button for each entrance', () => {
    const deleteButtons = screen.getAllByTestId('delete-btn');

    deleteButtons.forEach((deleteButton) => {
      expect(deleteButton).toBeInTheDocument();
    });
  });
  it('On click, the line should be deleted', () => {
    const deleteButtons = screen.getAllByTestId('delete-btn')[0];
    const deletedLine  = screen.getAllByTestId('value');
    
    userEvent.click(deleteButtons);
    const userDataSpans = screen.getAllByTestId('value');

    expect(userDataSpans.length).toBe(userData.length - 1);
    expect(deletedLine[1]).not.toBeInTheDocument();

    expect(userDataSpans[0]).toHaveTextContent('200');
  });
});
