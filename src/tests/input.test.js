import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import UserInput from '../components/UserInput';
import { data } from './mocks/data';

beforeEach(() => render(<UserInput currencies={data}/>))

describe('Should render user inputs', () => {
  it('There is a value input', () => {
    const input = screen.getByRole('spinbutton', { name: 'value' });

    expect(input).toBeInTheDocument();
    expect(input).toHaveValue(null);
    expect(input).toHaveAttribute('type', 'number');
  });
  it('There is a description input', () => {
    const description = screen.getByRole('textbox', { name: 'description' });

    expect(description).toBeInTheDocument();
    expect(description).toHaveValue('');
    expect(description).toHaveAttribute('type', 'text');
  });
  it('There is a currency option selector', () => {
    const currency = screen.getByRole('combobox', { name: 'currency' });

    expect(currency).toBeInTheDocument();
  });
  it('There is a button', () => {
    const button = screen.getByRole('button', { name: 'Add' });

    expect(button).toBeInTheDocument();
    expect(button).toHaveTextContent('Adicionar');
    expect(button).toHaveAttribute('type', 'button');
    expect(button).toBeDisabled();
  });
});

describe('Should user be able to add a new entrance', () => {
  it('Type a value', () => {
    const input = screen.getByRole('spinbutton', { name: 'value' });

    userEvent.type(input, '100');
    expect(input).toHaveValue(100);

    userEvent.type(input, '{selectall},{backspace}');
    userEvent.type(input, 'teste');
    expect(input).toHaveValue(null);
  });
  it('Type description', () => {
    const description = screen.getByRole('textbox', { name: 'description' });

    userEvent.type(description, 'teste');
    expect(description).toHaveValue('teste');

    userEvent.type(description, '{selectall},{backspace}');
    expect(description).toHaveValue('');
  });
  it('Select a currency', () => {
    const currency = screen.getByRole('combobox', { name: 'currency' });
    const option = screen.getByRole('option', { name: 'CAD' });
    userEvent.selectOptions(currency, option,);
    expect(currency).toHaveValue('CAD');
  });
  it('Clicking on button clear the inputs', () => {
    const button = screen.getByRole('button', { name: 'Add' });
    const input = screen.getByRole('spinbutton', { name: 'value' });
    const description = screen.getByRole('textbox', { name: 'description' });
    const currency = screen.getByRole('combobox', { name: 'currency' });
    const option = screen.getByRole('option', { name: 'CAD' });

    userEvent.type(input, '100');

    expect(button).toBeEnabled();

    userEvent.type(description, 'teste');
    userEvent.selectOptions(currency, option);

    userEvent.click(button);

    expect(input).toHaveValue(null);
    expect(description).toHaveValue('');
    expect(currency).toHaveValue('USD');
  });
});