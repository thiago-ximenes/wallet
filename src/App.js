import React, { useEffect, useState } from 'react'
import api from './services/api';
import UserInput from './components/UserInput.jsx';
import Table from './components/Table.jsx';

function App() {
  const [currencies, setCurrencies] = useState([]);
  const [loading, setLoading] = useState(true);
  const [userData, setUserData] = useState([]);
  useEffect(() => {
    api()
      .then((response) => {
        setCurrencies(response);
      })
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
  },[])

  function userInputs(value, description, currency) {
    setUserData([{ value, description, currency }]);
  }

  return (
    !loading ? (
    <div
      className="flex flex-col items-center justify-center mt-10"
    >
      <UserInput currencies={currencies} userInputs={userInputs}/>
      <Table apiData={currencies} userData={userData} />
    </div>
    ) : <h1 className="flex flex-col items-center justify-center mt-10">Loading...</h1>
  )
}

export default App