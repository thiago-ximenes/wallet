import axios from 'axios'

export default async function api() {
  try {
    const response = await axios.get('https://economia.awesomeapi.com.br/json/all');
    return response.data;
  } catch (error) {
    console.error(error);
  }
}